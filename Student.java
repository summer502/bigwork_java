package system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Component;

public class Student extends JFrame {
	String account;
	private JPanel contentPane;

	public Student(String account) {
		this.account = account;
		setVisible(true);
		setTitle("学生界面");
		setBounds(100, 100, 548, 332);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("查看个人信息");
		btnNewButton.setBounds(195, 50, 143, 39);
		contentPane.add(btnNewButton);

		JButton btnNewButton1 = new JButton("查看成绩");
		btnNewButton1.setBounds(195, 119, 143, 39);
		contentPane.add(btnNewButton1);

		JButton btnNewButton2 = new JButton("修改密码");
		btnNewButton2.setBounds(195, 190, 143, 39);
		contentPane.add(btnNewButton2);
		/*
		 * 查看学生个人信息事件动作监听
		 */
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Student_PersonalInformation();
			}
		});
		/*
		 * 查看学生个人成绩事件动作监听
		 */
		btnNewButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Student_Mark();
			}
		});
		/*
		 * 修改学生密码事件动作监听
		 */
		btnNewButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Student_Update();

			}
		});
	}

	/*
	 * 学生个人信息面板类
	 */
	class Student_PersonalInformation extends JFrame {
		Student_PersonalInformation() {
			setTitle("个人信息界面");
			setVisible(true);
			setBounds(200, 200, 522, 370);
			getContentPane().setLayout(null);

			JLabel label = new JLabel("个人信息");
			label.setFont(new Font("楷体", Font.BOLD, 30));
			label.setBounds(186, 24, 125, 35);
			getContentPane().add(label);

			JTextArea textArea = new JTextArea();
			textArea.setFont(new Font("楷体", Font.BOLD, 25));
			JScrollPane scrollPane = new JScrollPane(textArea);
			scrollPane.setBounds(48, 76, 411, 227);
			getContentPane().add(scrollPane);

			try {
				Connection con = SQL_Test.getConnection();
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from student_login where account='" + account + "'");
				textArea.setText("账号\t  密码\t   姓名\n");
				while (rs.next()) {
					textArea.append((rs.getString("account") + "  " + rs.getString("password") + "  "
							+ rs.getString("name") + "\n"));
				}
				new SQL_Test().close(con, stmt, rs);
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

		}
	}

	/*
	 * 学生个人成绩面板
	 */
	class Student_Mark extends JFrame {
		Student_Mark() {
			setTitle("个人成绩界面");
			setVisible(true);
			setBounds(200, 200, 522, 370);
			getContentPane().setLayout(null);

			JLabel label = new JLabel("个人成绩");
			label.setFont(new Font("楷体", Font.BOLD, 30));
			label.setBounds(186, 24, 125, 35);
			getContentPane().add(label);

			JTextArea textArea = new JTextArea();
			textArea.setFont(new Font("楷体", Font.BOLD, 25));
			JScrollPane scrollPane = new JScrollPane(textArea);
			scrollPane.setBounds(48, 76, 411, 227);
			getContentPane().add(scrollPane);

			try {
				Connection con = SQL_Test.getConnection();
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from student_login where account='" + account + "'");
				textArea.setText("语文\t数学\t英语\n");
				while (rs.next()) {
					textArea.append((rs.getString("chinese") + "\t" + rs.getString("math") + "\t"
							+ rs.getString("english") + "\n"));
				}
				new SQL_Test().close(con, stmt, rs);
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

		}
	}

	/*
	 * 学生修改密码面板
	 */
	class Student_Update extends JFrame {
		private JPasswordField passwordField;

		Student_Update() {
			setTitle("修改密码界面");
			setVisible(true);
			setBounds(400, 200, 440, 307);
			getContentPane().setLayout(null);

			JLabel lblNewLabel = new JLabel("请输入您的新密码");
			lblNewLabel.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel.setBounds(127, 40, 166, 42);
			getContentPane().add(lblNewLabel);

			passwordField = new JPasswordField();
			passwordField.setBounds(127, 100, 166, 36);
			getContentPane().add(passwordField);

			JButton btnNewButton = new JButton("确定修改");
			btnNewButton.setFont(new Font("宋体", Font.BOLD, 20));
			btnNewButton.setBounds(127, 170, 166, 42);
			getContentPane().add(btnNewButton);

			/*
			 * 学生修改密码事件动作监听
			 */
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Connection con = SQL_Test.getConnection();
						Statement stmt = con.createStatement();
						int rs = stmt.executeUpdate("update student_login set password='" + passwordField.getText()
								+ "' where account='" + account + "'");
						String str = "密码更改成功，新密码为" + passwordField.getText();
						if (rs != 0) {
							JOptionPane.showMessageDialog(null, str, "success", JOptionPane.INFORMATION_MESSAGE);
						}
						new SQL_Test().close(con, stmt, null);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}
			});
		}
	}
}
