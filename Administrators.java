package system;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.Font;

public class Administrators extends JFrame {
	String account;

	Administrators(String account) {
		this.account = account;
		init();
		setTitle("管理员界面");
		setBounds(200, 100, 553, 520);
		setVisible(true);
	}

	void init() {
		Container cp = getContentPane();
		getContentPane().setLayout(null);

		JButton btnNewButton1 = new JButton("修改密码");
		btnNewButton1.setFont(new Font("楷体", Font.BOLD, 14));
		btnNewButton1.setBounds(167, 30, 198, 44);
		getContentPane().add(btnNewButton1);

		JButton btnNewButton2 = new JButton("录入教师账号和密码");
		btnNewButton2.setFont(new Font("楷体", Font.BOLD, 14));
		btnNewButton2.setBounds(167, 90, 198, 44);
		getContentPane().add(btnNewButton2);

		JButton btnNewButton3 = new JButton("修改教师信息");
		btnNewButton3.setFont(new Font("楷体", Font.BOLD, 14));
		btnNewButton3.setBounds(167, 150, 198, 44);
		getContentPane().add(btnNewButton3);

		JButton btnNewButton4 = new JButton("删除教师信息");
		btnNewButton4.setFont(new Font("楷体", Font.BOLD, 14));
		btnNewButton4.setBounds(167, 210, 198, 44);
		getContentPane().add(btnNewButton4);

		JButton btnNewButton5 = new JButton("查看个人信息");
		btnNewButton5.setFont(new Font("楷体", Font.BOLD, 14));
		btnNewButton5.setBounds(167, 270, 198, 44);
		getContentPane().add(btnNewButton5);

		JButton btnNewButton6 = new JButton("查看所有教师账号信息");
		btnNewButton6.setFont(new Font("楷体", Font.BOLD, 14));
		btnNewButton6.setBounds(167, 330, 198, 44);
		getContentPane().add(btnNewButton6);

		JButton btnNewButton7 = new JButton("按姓名查找某个教师信息");
		btnNewButton7.setFont(new Font("楷体", Font.BOLD, 14));
		btnNewButton7.setBounds(167, 390, 198, 44);
		getContentPane().add(btnNewButton7);

		/*
		 * 修改密码按钮添加动作事件监听
		 */
		btnNewButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Administrators_Update();
			}
		});
		/*
		 * 录入教师账号密码按钮添加动作事件监听
		 */
		btnNewButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Administrators_Insert();
			}
		});
		/*
		 * 修改教师信息按钮添加动作事件监听
		 * */
		btnNewButton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Administrators_Update1();
			}
		});
		/*
		 * 删除教师信息按钮添加动作事件监听
		 * */
		btnNewButton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Administrators_Delete();
			}
		});
		/*
		 * 查看个人信息按钮添加动作事件监听
		 */
		btnNewButton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Administrators_PersonalInformation();
			}
		});
		/*
		 * 查看所有教师账号信息按钮添加动作事件监听
		 */
		btnNewButton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Administrators_AllSelect();
			}
		});
		/*
		 * 按姓名查找某个教师信息按钮添加动作事件监听
		 * */
		btnNewButton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Administrators_Select();
			}
		});
	}

	/*
	 * 管理员修改密码类
	 */
	class Administrators_Update extends JFrame {
		private JPasswordField passwordField;

		Administrators_Update() {
			setTitle("修改密码界面");
			setVisible(true);
			setBounds(400, 200, 440, 307);
			getContentPane().setLayout(null);

			JLabel lblNewLabel = new JLabel("请输入您的新密码");
			lblNewLabel.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel.setBounds(127, 40, 166, 42);
			getContentPane().add(lblNewLabel);

			passwordField = new JPasswordField();
			passwordField.setBounds(127, 100, 166, 36);
			getContentPane().add(passwordField);

			JButton btnNewButton = new JButton("确定修改");
			btnNewButton.setFont(new Font("宋体", Font.BOLD, 20));
			btnNewButton.setBounds(127, 170, 166, 42);
			getContentPane().add(btnNewButton);

			/*
			 * 管理员修改密码事件动作监听
			 */
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Connection con = SQL_Test.getConnection();
						Statement stmt = con.createStatement();
						int rs = stmt.executeUpdate("update administrators_login set password='"
								+ passwordField.getText() + "' where account='" + account + "'");
						String str = "密码更改成功，新密码为" + passwordField.getText();
						if (rs != 0) {
							JOptionPane.showMessageDialog(null, str, "success", JOptionPane.INFORMATION_MESSAGE);
						}
						new SQL_Test().close(con, stmt, null);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}
			});
		}
	}

	/*
	 * 管理员录入教师信息类
	 */
	class Administrators_Insert extends JFrame {
		private JPasswordField passwordField;
		private JTextField textField;
		private JTextField textField2;

		Administrators_Insert() {
			setTitle("录入教师信息界面");
			setVisible(true);
			setBounds(200, 200, 436, 323);
			getContentPane().setLayout(null);

			JLabel lblNewLabel = new JLabel("请输入要录入教师的账号,密码和姓名");
			lblNewLabel.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel.setBounds(49, 10, 355, 53);
			getContentPane().add(lblNewLabel);

			passwordField = new JPasswordField();
			passwordField.setFont(new Font("宋体", Font.BOLD, 20));
			passwordField.setBounds(139, 119, 184, 36);
			getContentPane().add(passwordField);

			textField = new JTextField();
			textField.setFont(new Font("宋体", Font.BOLD, 20));
			textField.setBounds(139, 73, 184, 36);
			getContentPane().add(textField);
			textField.setColumns(10);

			textField2 = new JTextField();
			textField2.setFont(new Font("宋体", Font.BOLD, 20));
			textField2.setBounds(139, 165, 184, 36);
			getContentPane().add(textField2);
			textField2.setColumns(10);

			JLabel NewLabel1 = new JLabel("账号");
			NewLabel1.setFont(new Font("宋体", Font.BOLD, 20));
			NewLabel1.setBounds(87, 73, 42, 36);
			getContentPane().add(NewLabel1);

			JLabel NewLabel2 = new JLabel("密码");
			NewLabel2.setFont(new Font("宋体", Font.BOLD, 20));
			NewLabel2.setBounds(87, 119, 42, 36);
			getContentPane().add(NewLabel2);

			JLabel NewLabel3 = new JLabel("姓名");
			NewLabel3.setFont(new Font("宋体", Font.BOLD, 20));
			NewLabel3.setBounds(87, 164, 42, 33);
			getContentPane().add(NewLabel3);

			JButton btnNewButton = new JButton("确定录入");
			btnNewButton.setFont(new Font("宋体", Font.BOLD, 20));
			btnNewButton.setBounds(126, 218, 166, 42);
			getContentPane().add(btnNewButton);

			/*
			 * 教师信息录入按钮动作事件监听
			 */
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Connection con = SQL_Test.getConnection();
						PreparedStatement pstmt = con.prepareStatement("insert into teacher_login values(?,?,?)");
						pstmt.setString(1, textField.getText());
						pstmt.setString(2, passwordField.getText());
						pstmt.setString(3, textField2.getText());
						int i = pstmt.executeUpdate();
						if (i != 0) {
							JOptionPane.showMessageDialog(null, "录入成功", "success", JOptionPane.INFORMATION_MESSAGE);
						}
						new SQL_Test().close(con, pstmt, null);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "该账号已存在，录入失败", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
	}

	/*
	 * 管理员个人信息面板类
	 */
	class Administrators_PersonalInformation extends JFrame {
		Administrators_PersonalInformation() {
			setTitle("个人信息界面");
			setVisible(true);
			setBounds(200, 200, 522, 370);
			getContentPane().setLayout(null);

			JLabel label = new JLabel("个人信息");
			label.setFont(new Font("楷体", Font.BOLD, 30));
			label.setBounds(186, 24, 125, 35);
			getContentPane().add(label);

			JTextArea textArea = new JTextArea();
			textArea.setFont(new Font("楷体", Font.BOLD, 25));
			JScrollPane scrollPane = new JScrollPane(textArea);
			scrollPane.setBounds(48, 76, 411, 227);
			getContentPane().add(scrollPane);

			try {
				Connection con = SQL_Test.getConnection();
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from administrators_login where account='" + account + "'");
				textArea.setText("账号\t  密码\t   姓名\n");
				while (rs.next()) {
					textArea.append((rs.getString("account") + "  " + rs.getString("password") + "  "
							+ rs.getString("name") + "\n"));
				}
				new SQL_Test().close(con, stmt, rs);
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

		}
	}

	/*
	 * 查看所有教师账号信息类
	 */
	class Administrators_AllSelect extends JFrame {
		public Administrators_AllSelect() {

			setTitle("查看教师信息界面");
			setVisible(true);
			setBounds(200, 200, 430, 412);
			getContentPane().setLayout(null);

			JTextArea textArea = new JTextArea();
			textArea.setBounds(10, 168, 396, 197);
			textArea.setFont(new Font("楷体", Font.BOLD, 25));
			textArea.setText("账号\t  密码\t   姓名\n");
			JScrollPane scrollPane = new JScrollPane(textArea);
			scrollPane.setBounds(10, 64, 396, 301);
			getContentPane().add(scrollPane);

			JLabel lblNewLabel = new JLabel("以下为所有教师信息");
			lblNewLabel.setFont(new Font("宋体", Font.BOLD, 30));
			lblNewLabel.setBounds(67, 10, 297, 44);
			getContentPane().add(lblNewLabel);
			try {
				Connection con = SQL_Test.getConnection();
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from teacher_login");
				while (rs.next()) {
					textArea.append(rs.getString("account") + "  " + rs.getString("password") + "  "
							+ rs.getString("name") + "\n");
				}
				new SQL_Test().close(con, stmt, rs);
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	/*
	 * 管理员修改教师信息类
	 * */
	class Administrators_Update1 extends JFrame {
		private JTextField textField;
		private JTextField textField1;
		private JPasswordField password;
		Administrators_Update1() {
			setTitle("修改教师信息界面");
			setVisible(true);
			setBounds(200, 200, 473, 290);
			getContentPane().setLayout(null);

			JLabel lblNewLabel = new JLabel("请输入要修改教师的账号和信息");
			lblNewLabel.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel.setBounds(87, 24, 291, 36);
			getContentPane().add(lblNewLabel);

			textField = new JTextField();
			textField.setFont(new Font("宋体", Font.BOLD, 20));
			textField.setBounds(139, 70, 231, 36);
			getContentPane().add(textField);
			textField.setColumns(10);

			JLabel lblNewLabel_1 = new JLabel("账号");
			lblNewLabel_1.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel_1.setBounds(87, 70, 42, 36);
			getContentPane().add(lblNewLabel_1);

			JLabel lblNewLabel_2 = new JLabel("姓名");
			lblNewLabel_2.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel_2.setBounds(87, 120, 42, 36);
			getContentPane().add(lblNewLabel_2);

			JLabel lblNewLabel_3 = new JLabel("密码");
			lblNewLabel_3.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel_3.setBounds(87, 170, 42, 33);
			getContentPane().add(lblNewLabel_3);

			textField1 = new JTextField();
			textField1.setFont(new Font("宋体", Font.BOLD, 20));
			textField1.setBounds(139, 120, 92, 36);
			getContentPane().add(textField1);
			textField1.setColumns(10);

			password = new JPasswordField();
			password.setFont(new Font("宋体", Font.BOLD, 20));
			password.setBounds(139, 170, 92, 36);
			getContentPane().add(password);
			password.setColumns(10);

			JButton btnNewButton1 = new JButton("确定修改");
			btnNewButton1.setFont(new Font("宋体", Font.BOLD, 20));
			btnNewButton1.setBounds(243, 116, 127, 40);
			getContentPane().add(btnNewButton1);

			JButton btnNewButton2 = new JButton("确定修改");
			btnNewButton2.setFont(new Font("宋体", Font.BOLD, 20));
			btnNewButton2.setBounds(243, 168, 127, 40);
			getContentPane().add(btnNewButton2);
			
			/*
			 * 修改教师姓名按钮添加动作事件监听
			 * */
			btnNewButton1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Connection con = SQL_Test.getConnection();
						PreparedStatement pstmt = con
								.prepareStatement("update teacher_login set name=?where account=?");
						pstmt.setString(2, textField.getText());
						pstmt.setString(1, textField1.getText());
						int i = pstmt.executeUpdate();
						if (i != 0) {
							JOptionPane.showMessageDialog(null, "修改成功", "success", JOptionPane.INFORMATION_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(null, "账号不存在，修改失败", "Error", JOptionPane.ERROR_MESSAGE);
						}
						new SQL_Test().close(con, pstmt, null);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
			/*
			 * 修改教师密码按钮添加动作事件监听
			 * */
			btnNewButton2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Connection con = SQL_Test.getConnection();
						PreparedStatement pstmt = con
								.prepareStatement("update teacher_login set password=?where account=?");
						pstmt.setString(2, textField.getText());
						pstmt.setString(1, password.getText());
						int i = pstmt.executeUpdate();
						if (i != 0) {
							JOptionPane.showMessageDialog(null, "修改成功，账号为"+account+"的教师的新密码为"+password.getText(), "success", JOptionPane.INFORMATION_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(null, "账号不存在，修改失败", "Error", JOptionPane.ERROR_MESSAGE);
						}
						new SQL_Test().close(con, pstmt, null);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
			}	
	}
	/*
	 * 教师信息删除类
	 * */
	class Administrators_Delete extends JFrame{
		JTextField textField;
		Administrators_Delete(){
			setTitle("教师信息删除界面");
			setVisible(true);
			setBounds(200, 200, 431, 232);
			getContentPane().setLayout(null);

			JLabel lblNewLabel = new JLabel("请输入要删除的教师账号");
			lblNewLabel.setFont(new Font("宋体", Font.BOLD, 25));
			lblNewLabel.setBounds(71, 18, 282, 42);
			getContentPane().add(lblNewLabel);

			textField = new JTextField();
			textField.setFont(new Font("宋体", Font.BOLD, 20));
			textField.setBounds(132, 70, 184, 36);
			getContentPane().add(textField);
			textField.setColumns(10);

			JLabel lblNewLabel_1 = new JLabel("账号");
			lblNewLabel_1.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel_1.setBounds(87, 70, 42, 36);
			getContentPane().add(lblNewLabel_1);

			JButton btnNewButton = new JButton("确定删除");
			btnNewButton.setFont(new Font("宋体", Font.BOLD, 20));
			btnNewButton.setBounds(132, 126, 132, 42);
			getContentPane().add(btnNewButton);
			
			/*
			 * 删除按钮添加动作事件监听
			 * */
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Connection con = SQL_Test.getConnection();
						PreparedStatement pstmt = con
								.prepareStatement("delete from teacher_login where account=?");
						pstmt.setString(1, textField.getText());
						int i = pstmt.executeUpdate();
						if (i != 0) {
							JOptionPane.showMessageDialog(null, "账号为"+textField.getText()+"的教师信息已删除","success", JOptionPane.INFORMATION_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(null, "账号不存在，删除失败", "Error", JOptionPane.ERROR_MESSAGE);
						}
						new SQL_Test().close(con, pstmt, null);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
		}
	}
	/*
	 * 按姓名查找教师信息
	 * */
	class Administrators_Select extends JFrame {
		private JTextField textField;

		Administrators_Select() {
			setTitle("查询成绩界面");
			setVisible(true);
			setBounds(200, 200, 430, 412);
			getContentPane().setLayout(null);

			JLabel lblNewLabel = new JLabel("请输入要查询的教师姓名");
			lblNewLabel.setFont(new Font("宋体", Font.BOLD, 25));
			lblNewLabel.setBounds(71, 18, 282, 42);
			getContentPane().add(lblNewLabel);

			textField = new JTextField();
			textField.setFont(new Font("宋体", Font.BOLD, 20));
			textField.setBounds(132, 70, 184, 36);
			getContentPane().add(textField);
			textField.setColumns(10);

			JLabel lblNewLabel_1 = new JLabel("姓名");
			lblNewLabel_1.setFont(new Font("宋体", Font.BOLD, 20));
			lblNewLabel_1.setBounds(87, 70, 42, 36);
			getContentPane().add(lblNewLabel_1);

			JButton btnNewButton = new JButton("查询");
			btnNewButton.setFont(new Font("宋体", Font.BOLD, 20));
			btnNewButton.setBounds(142, 116, 116, 42);
			getContentPane().add(btnNewButton);

			JTextArea textArea = new JTextArea();
			textArea.setBounds(10, 168, 396, 197);
			textArea.setFont(new Font("楷体", Font.BOLD, 25));
			textArea.setText("账号\t  密码\t   姓名\n");
			JScrollPane scrollPane = new JScrollPane(textArea);
			scrollPane.setBounds(10, 168, 396, 197);
			getContentPane().add(scrollPane);

			/*
			 * 按姓名查询教师信息按钮动作事件监听
			 */
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int a = 0;
					try {
						Connection con = SQL_Test.getConnection();
						Statement stmt = con.createStatement();
						String name = textField.getText();
						ResultSet rs = stmt.executeQuery("select * from teacher_login where name='" + name + "'");
						while (rs.next()) {
							textArea.append(rs.getString("account") + "  " + rs.getString("password") + "  "
									+ rs.getString("name") + "\n");
							a++;
						}
						if (a == 0) {
							JOptionPane.showMessageDialog(null, "姓名不存在，请重新查询", "Error", JOptionPane.ERROR_MESSAGE);
						}
						new SQL_Test().close(con, stmt, rs);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
		}
	}
}