package system;

import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	Login() {
		setTitle("登录界面");
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel Label = new JLabel("账号");
		Label.setFont(new Font("宋体", Font.PLAIN, 30));
		Label.setBounds(171, 64, 61, 42);
		contentPane.add(Label);

		JLabel Labe2 = new JLabel("密码");
		Labe2.setFont(new Font("宋体", Font.PLAIN, 30));
		Labe2.setBounds(171, 132, 61, 42);
		contentPane.add(Labe2);

		textField = new JTextField(); // 输入账号的文本框
		textField.setFont(new Font("宋体", Font.BOLD, 25));
		textField.setBounds(242, 64, 195, 42);
		contentPane.add(textField);
		textField.setColumns(10);

		passwordField = new JPasswordField(); // 密码框
		passwordField.setFont(new Font("宋体", Font.BOLD, 25));
		passwordField.setBounds(242, 132, 195, 42);
		contentPane.add(passwordField);

		ButtonGroup group = new ButtonGroup();
		JRadioButton radioButton = new JRadioButton("管理员");
		radioButton.setFont(new Font("宋体", Font.PLAIN, 25));
		radioButton.setBounds(154, 203, 106, 30);
		contentPane.add(radioButton);

		JRadioButton radioButton1 = new JRadioButton("教师");
		radioButton1.setFont(new Font("宋体", Font.PLAIN, 25));
		radioButton1.setBounds(286, 203, 85, 30);
		contentPane.add(radioButton1);

		JRadioButton radioButton2 = new JRadioButton("学生");
		radioButton2.setFont(new Font("宋体", Font.PLAIN, 25));
		radioButton2.setBounds(401, 203, 85, 30);
		contentPane.add(radioButton2);

		/*
		 * 给单选按钮添加按钮组
		 */
		group.add(radioButton);
		group.add(radioButton1);
		group.add(radioButton2);

		JButton btnNewButton = new JButton("登录");
		btnNewButton.setFont(new Font("宋体", Font.PLAIN, 30));
		btnNewButton.setBounds(200, 259, 220, 52);
		contentPane.add(btnNewButton);

		/*
		 * 登录按钮添加动作事件监听
		 */
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = textField.getText();
				String str2 = passwordField.getText();

				/*
				 * 管理员登录事件监听
				 */
				if (radioButton.isSelected()) {
					try {
						boolean flag = false;
						String account = null;
						Connection con = SQL_Test.getConnection();
						Statement stmt = con.createStatement();
						ResultSet rs = stmt
								.executeQuery("select * from administrators_login where account='" + str + "'");
						while (rs.next()) {
							if (rs.getString("password").equalsIgnoreCase(str2)) {
								flag = true;
								account = rs.getString("account");
								break;
							}
						}
						if (flag == true) {
							JOptionPane.showMessageDialog(null, "登录成功！该用户是管理员", "success",
									JOptionPane.INFORMATION_MESSAGE);
							new Administrators(account);
						} else {
							JOptionPane.showMessageDialog(null, "账号或密码错误！", "Error", JOptionPane.ERROR_MESSAGE);
						}

						SQL_Test.close(con, stmt, rs);
					} catch (ClassNotFoundException e1) {

						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}

				/*
				 * 教师登录事件监听
				 */
				else if (radioButton1.isSelected()) {
					try {
						boolean flag = false;
						String account = null;
						Connection con = SQL_Test.getConnection();
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery("select * from teacher_login where account='" + str + "'");
						while (rs.next()) {
							if (rs.getString("password").equalsIgnoreCase(str2)) {
								flag = true;
								account = rs.getString("account");
								break;
							}
						}
						if (flag == true) {
							JOptionPane.showMessageDialog(null, "登录成功！该用户是教师", "success",
									JOptionPane.INFORMATION_MESSAGE);
							new Teacher(account);
						} else {
							JOptionPane.showMessageDialog(null, "账号或密码错误！", "Error", JOptionPane.ERROR_MESSAGE);
						}

						SQL_Test.close(con, stmt, rs);
					} catch (ClassNotFoundException e1) {

						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}

				/*
				 * 学生登录事件监听
				 */
				else if (radioButton2.isSelected()) {
					try {
						boolean flag = false;
						String account = null;
						Connection con = SQL_Test.getConnection();
						Statement stmt = con.createStatement();
						ResultSet rs = stmt.executeQuery("select * from student_login where account='" + str + "'");
						while (rs.next()) {
							if (rs.getString("password").equalsIgnoreCase(str2)) {
								account = rs.getString("account");
								flag = true;
								break;
							}
						}
						if (flag == true) {
							JOptionPane.showMessageDialog(null, "登录成功！该用户是学生", "success",
									JOptionPane.INFORMATION_MESSAGE);
							new Student(account);
						} else {
							JOptionPane.showMessageDialog(null, "账号或密码错误！", "Error", JOptionPane.ERROR_MESSAGE);
						}

						SQL_Test.close(con, stmt, rs);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				} else {
					JOptionPane.showMessageDialog(null, "请选择用户类型！", "Error", JOptionPane.ERROR_MESSAGE);
				}

			}
		});

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new Login();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
